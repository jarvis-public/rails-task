class UserMailer < ApplicationMailer
  default from: "shubham.ipec2013@gmail.com"

  def user_email(user)
    @user = user
    mail(to: @user.email, subject: 'Email confirmation')
  end
end
