class ContactsController < ApplicationController

  before_action :authorize

  def index
    @contacts = Contact.where(user_id: current_user.id).order(:created_at => "DESC")
  end

  def create

    @contact = Contact.new(contact_params)
    if params[:contact][:name] == ''
      flash[:warning] = "Please enter name."
    end
    if params[:contact][:name] != '' && params[:contact][:phone_no] == ''
      flash[:warning] = "Please enter phone number."
    end
    if params[:contact][:name] != '' && params[:contact][:phone_no] != '' && @contact.save
      redirect_to contacts_index_path
    else
      redirect_to controller: 'contacts', action: 'new'
    end
  end

  def new
    @contact = Contact.new
  end

  def edit
    @contact = Contact.find(params[:id])
  end

  def extended
    @usercontacts = Contact.select(:phone_no).where(user_id: current_user.id)
    @usercontactids = Contact.where("user_id IN (?) and phone_no!=?", User.select(:id).where("mobile IN (?)", @usercontacts),current_user.mobile)
  end

  def update
    @contact = Contact.find(params[:id])
    @contact.update(contact_params)
    redirect_to contacts_index_path
  end

  def destroy
    @contact = Contact.find(params[:id])
    @contact.destroy

    redirect_to contacts_index_path
  end

  private def contact_params
    params.require(:contact).permit(:name, :phone_no, :user_id)
  end

end
