class UsersController < ApplicationController

  before_action :authorize

  def index
    @users = User.all
  end

  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save

        # Sends email to user when user is created.
        UserMailer.user_email(@user).deliver

        format.html {redirect_to '/', notice: 'User was successfully created.'}
        format.json {render :show, status: :created, location: @user}
      else
        format.html {render :new}
        format.json {render json: @user.errors, status: :unprocessable_entity}
      end
    end
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    @user.update(user_params)

    redirect_to controller: 'users', action: 'index'
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy

    redirect_to controller: 'users', action: 'index'
  end

  private def user_params
    params.require(:user).permit(:name, :email, :mobile, :password, :password_confirmation)
  end
end
