class SessionsController < ApplicationController

  require 'digest/md5'

  def create
    if params[:email] == ''
      flash[:warning] = "Please enter email."
    end
    if params[:email] != '' && params[:password] == ''
      flash[:warning] = "Please enter password."
    end
    user = User.find_by_email(params[:email])
    # If the user exists AND the password entered is correct.
    if user && user.authenticate(params[:password]) && user.email_verified
      # Save the user id inside the browser cookie. This is how we keep the user
      # logged in when they navigate around our website.
      session[:user_id] = user.id
      redirect_to '/'
    else
      if !user.email_verified
        flash[:warning] = "Your email is not verified."
      else
        flash[:warning] = "Invalid login credentials."
      end
      redirect_to '/login'
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to '/login'
  end

  def signup
    @user = User.new
  end

  def emailverification
    user = User.find_by_email(params[:token])
    if user && user.update_attribute(:email_verified, 1)
      flash[:warning] = "Your email verified,You can login now."
      redirect_to '/login'
    else
      flash[:warning] = "Unable to verify your email."
      redirect_to '/signup'
    end
  end

  def forgotpassword

  end

  def resetpasswordchange
    if !params[:user] || !params[:user][:reset_token] || params[:user][:reset_token] == ''
      redirect_to '/login'
    else
      user = User.find_by_reset_token(params[:user][:reset_token])
      if user && user.update_attribute(:password, params[:user][:password])
        flash[:warning] = "Password reset successfully."
        redirect_to '/login'
      else
        flash[:warning] = "User not found."
        redirect_to '/signup'
      end
    end
  end

  def sendforgotpasswordmail
    user = User.find_by_email(params[:email])
    if user && user.update_attribute(:reset_token, Digest::MD5.hexdigest(user.email + user.name + user.mobile))
      ResetPasswordMailer.reset_password_email(user).deliver
      flash[:warning] = "Reset password link has been sent to your email."
      redirect_to '/login'
    else
      flash[:warning] = "Email not found."
      redirect_to forgot_password_path
    end
  end

  def resetpassword
    user = User.find_by_reset_token(params[:token])
    if user
      @token = params[:token]
    else
      flash[:warning] = "Account not found."
      redirect_to '/login'
    end
  end

  def signupuser
    @allowtoregister = true
    @user = User.new(user_params)
    if params[:user][:password] != params[:user][:password_confirmation]
      flash[:warning] = "Password and confirm password didn't match."
      @allowtoregister = false
    end
    if params[:user][:password] == ''
      flash[:warning] = "Password is required."
      @allowtoregister = false
    end
    if params[:user][:mobile] == ''
      flash[:warning] = "Mobile number is required."
      @allowtoregister = false
    end
    if params[:user][:email] == ''
      flash[:warning] = "Email is required."
      @allowtoregister = false
    end
    if params[:user][:name] == ''
      flash[:warning] = "Name is required."
      @allowtoregister = false
    end
    if User.find_by_email(params[:user][:email])
      flash[:warning] = "Email already exist."
      @allowtoregister = false
    end
    respond_to do |format|
      if @allowtoregister && @user.save
        UserMailer.user_email(@user).deliver

        format.html {redirect_to '/login', notice: 'User created successfully.Please verify your email to login.'}
      else
        format.html {render :new}
      end
    end
  end

  private def user_params
    params.require(:user).permit(:name, :email, :mobile, :password, :password_confirmation)
  end
end
