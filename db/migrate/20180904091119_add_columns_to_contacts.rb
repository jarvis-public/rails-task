class AddColumnsToContacts < ActiveRecord::Migration[5.1]
  def change
    add_column :contacts, :name, :string
    add_column :contacts, :phone_no, :string
  end
end
