Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'contacts#index', as: 'home'
  resource :users
  resource :contacts
  get '/contacts/index' => 'contacts#index'
  get '/contacts/extended' => 'contacts#extended'
  get '/email/confirmation' => 'sessions#emailverification'
  get '/forgot/password' => 'sessions#forgotpassword'
  post '/forgot/password' => 'sessions#sendforgotpasswordmail'
  get '/reset/password' => 'sessions#resetpassword'
  post '/reset/password' => 'sessions#resetpasswordchange'
  get '/login' => 'sessions#new'
  post '/login' => 'sessions#create'
  post '/signup' => 'sessions#signupuser'
  get '/logout' => 'sessions#destroy'
  get '/signup' => 'sessions#signup'
end
